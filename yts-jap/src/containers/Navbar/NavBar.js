import React,{Component} from 'react';
import Logo from "../../components/Logo/Logo";

class NavBar extends Component {
    render(){
        return (
            <nav>
                <Logo>Rambilas</Logo>
                <div className= 'nav-contents'>
                <NavItems navItems={this.props.navItems}/>
                </div>
            </nav>
        )
    }
};

const NavItems= (props)=>{
    return (
        <div className={'nav-items'}>
            {props.navItems.map(
                (navItem)=>(<NavItem label={navItem}/>)
                )}
        </div>
    )
};

const NavItem= (props)=>{
    return (
        <div className="nav-item">{props.label}</div>
    )
}
export default NavBar;