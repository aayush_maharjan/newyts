import React from 'react';
import logo from './logo.svg';
import './App.scss';
import Layout from './views/Layout/Layout'
import Home from './views/Home'
import Search from './views/Search'
import Labrat from './components/Labrat/Labrat'
import Movie from './components/Movie/Movie'

function App() {
  return (

    <div className="App">
      <Layout/>
      <Home/>
      <Search/>
      <Labrat/>
      <Movie/>
    </div>
  );
}

export default App;
